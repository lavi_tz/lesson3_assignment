#include "sqlite3.h"
#include <string>
#include <iostream>
#define CARPRICE 3
using namespace std;
int callback(void* notUsed, int argc, char** argv, char** azCol);
int callbackcar(void* notUsed, int argc, char** argv, char** azCol);
int callbackbuyer(void* notUsed, int argc, char** argv, char** azCol);
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);
int test(char *zErrMsg, int rc);
int carPrice = 0;
int buyerBalance = 0;
int canbuy=0;
int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	
	if (test(zErrMsg,rc))
	{
		return 1;
	}
	bool a = carPurchase(12, 3, db, zErrMsg);// works
	a = carPurchase(9, 18, db, zErrMsg);//works 
	a = carPurchase(10, 15, db, zErrMsg);//fails
	balanceTransfer(1, 2, 10000000, db, zErrMsg);
	system("PAUSE");
}
/*
a test function to see that all the sql querys went well 
*/
int test(char *zErrMsg, int rc)
{
	if (rc != SQLITE_OK) // test if every thing is ok 
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return 1;
	}
	return 0;
}
/*
callback for the buyer to get the amount of money he has 
*/
int callbackbuyer(void* notUsed, int argc, char** argv, char** azCol)
{
	buyerBalance = atoi(argv[argc-1]);
	return 0;
}
/*
 call back for the cars to get the price and if available
 */
int callbackcar(void* notUsed, int argc, char** argv, char** azCol)
{
	carPrice = atoi(argv[CARPRICE]);
	canbuy = atoi(argv[argc-1]);
	return 0;
}/*
 regular call back dosent do any thing
 */
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	return 0;
}
/*
this function enables buying a car
intput:int buyerid, int carid, sqlite3* db, char* zErrMsg
ouput : bool ( if bought or not )
*/
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc = 0;
	string msg = "SELECT * from accounts WHERE id ="+to_string(buyerid);// so you can add the vars to the query
	string msg2 = "SELECT * from cars WHERE id =" + to_string(carid);// so you can add the vars to the query
	rc = sqlite3_exec(db,msg.c_str(),callbackbuyer,0, &zErrMsg);
	if (test(zErrMsg, rc))
	{
		return false;
	}
	rc = sqlite3_exec(db,msg2.c_str(), callbackcar, 0, &zErrMsg);
	if (test(zErrMsg, rc))
	{
		return false;
	}
	if (canbuy != 1|| carPrice > buyerBalance)
	{
		return false;
	}
	int payment = buyerBalance - carPrice;
	rc = sqlite3_exec(db, "begin transaction", callback, 0, &zErrMsg);
	msg = "UPDATE accounts SET balance ="+to_string(payment)+ " WHERE id = "+to_string(buyerid);// so you can add the vars to the query
	rc = sqlite3_exec(db, msg.c_str(), callback, 0, &zErrMsg);
	if (test(zErrMsg, rc))
	{
		cout << "Failed to complete purchase try again";
		return false;
	}
	msg2 = "UPDATE cars SET available = 0 WHERE id = " + to_string(carid);// so you can add the vars to the query
	rc = sqlite3_exec(db, msg2.c_str(), callback, 0, &zErrMsg);
	if (test(zErrMsg, rc))
	{
		cout << "Failed to complete purchase try again";
		return false;
	}
	rc = sqlite3_exec(db,"commit", callback, 0, &zErrMsg);
	cout << "Enjoy Your new Vechile" << endl;
	return true;
}
/*
this function transfers money between accounts 
intput:int from, int to, int amount, sqlite3* db, char* zErrMsg
ouput : bool ( if transfered or not ) 
*/
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc = 0;
	string msg = "SELECT * from accounts WHERE id =" + to_string(from); // so you can add the vars to the query
	rc = sqlite3_exec(db, msg.c_str(), callbackbuyer, 0, &zErrMsg);
	int firstAcc = buyerBalance;
	msg = "SELECT * from accounts WHERE id =" + to_string(to);// so you can add the vars to the query
	rc = sqlite3_exec(db, msg.c_str(), callbackbuyer, 0, &zErrMsg);
	if (firstAcc < amount)
	{
		cout << "Transfer failed " << endl;
		return false;
	}
	rc = sqlite3_exec(db, "begin transaction", callback, 0, &zErrMsg);
	firstAcc = firstAcc - amount;
	msg = "UPDATE accounts SET balance =" + to_string(firstAcc) + " WHERE id = " + to_string(from); // so you can add the vars to the query
	rc = sqlite3_exec(db, msg.c_str(), callback, 0, &zErrMsg);
	buyerBalance += amount;
	msg = "UPDATE accounts SET balance =" + to_string(firstAcc) + " WHERE id = " + to_string(from);// so you can add the vars to the query
	rc = sqlite3_exec(db, msg.c_str(), callback, 0, &zErrMsg);
	rc = sqlite3_exec(db, "commit", callback, 0, &zErrMsg);
	cout << "You transfered :" << amount << " from :" << from << " to :" << to << endl;
	


}