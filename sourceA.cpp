#include "sqlite3.h"
#include <string>
#include <iostream>
using namespace std;
int callback(void* notUsed, int argc, char** argv, char** azCol);

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	// creating the table
	rc = sqlite3_exec(db, "CREATE TABLE people(id integer PRIMARY KEY AUTOINCREMENT,name VARCHAR )", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) // test if every thing is ok 
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	// inserting the first value 
	rc = sqlite3_exec(db, "INSERT INTO people(name) VALUES('lavi')", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)// test if every thing is ok 
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	// inserting the second value 
	rc = sqlite3_exec(db, "INSERT INTO people(name) VALUES('liroy')", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)// test if every thing is ok 
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	// inserting the third value 
	rc = sqlite3_exec(db, "INSERT INTO people(name) VALUES('eli')", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)// test if every thing is ok
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	rc = sqlite3_exec(db, "UPDATE people SET name = 'NewEli' where id = 3 ", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)// test if every thing is ok
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	system("PAUSE");
}
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	return 0;
}